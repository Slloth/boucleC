﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boucle_5
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] week = new string[] { "lundi", "mardi", "mercredie", "jeudi", "vendredi", "samedie", "dimanche" };

            //boucle Pour, qui possède comme instruction
            //( tant que i n'a pas la longeur du tableau "week" on incrémente i+1)
            for (int i = 0; i < week.Length; i++)
            {
                Console.WriteLine(week[i]);     //Tableau
            }
        }
    }
}
