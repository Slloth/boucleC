﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boucle_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            while (a < 10)
            {
                Console.WriteLine("Bonjour, je suis le message n°"+(a+1)+".");
                a++;
            }
        }
    }
}
