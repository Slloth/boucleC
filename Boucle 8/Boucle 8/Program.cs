﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boucle_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 234, 233, 255, 989, 765, 444, 234, 18, 19, 20 };
            foreach (int number in numbers)
            {
                if (number % 2 == 0)    //La condition vérifie après la division de la variable number, si il y'a un rest égal à 0 alors le chiffre est paire (on vérifie le reste d'une division avec Modulo'%')
                {
                    Console.WriteLine(number);
                }
            }
        }
    }
}
