﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boucle_6
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] months = new string[] { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juilliet", "Août", "Septembre", "Octobre","Novembre", "Decembreé"};
            //Boucle Pour, qui possède comme instruction ( tant que i n'a pas la longeur du tableau "week" on incrémente i+1)
            //On crée la string month dans la viarable du tableau months
            foreach (string month in months)
            {
                Console.WriteLine(month);       //Tableau
            }
        }
    }
}
